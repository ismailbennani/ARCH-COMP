function res = test_ellipsoid_enclose
% test_ellipsoid_enclose - unit test function of enclose
%
% Syntax:  
%    res = test_ellipsoid_enclose
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      26-July-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
%We need at least one non-degenerate ellipsoid to ensure the union of E1,E2 is not
%the empty set.
passed = true;
n = ceil(abs(3*randn));
E1 = ellipsoid.generate(false,n);
E2 = ellipsoid.generate(n);
E = enclose(E1,E2);
N = 1000;
S1 = sample(E1,N);
S2 = sample(E2,N);
%Check if the enclosed ellipsoid contains all sample points from both E1,E2
for i=1:N
    if ~contains(E,S1(:,i)) || ~contains(E,S2(:,i))
        passed = false;
    end
end
if passed
    disp('test_ellipsoid_mtimes successful');
else
    disp('test_ellipsoid_mtimes failed');
end
%------------- END OF CODE --------------
